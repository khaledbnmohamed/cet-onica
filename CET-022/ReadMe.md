CET-022

# Task Summary

-  Create an EKS cluster and the required IAM roles through the AWS Console.
-  Create Kubernetes pod (using YAML file) and use the following docker image gcr.io/google-samples/hello-app:2.0
-  Create Kubernetes service (using YAML file) for the created pods and expose it publicly to a LoadBalancer through port 8080.


# Diagram

![image info](schema.png)

# CloudFormation resources

  - EC2 instance
  - Security Group
  - IAM Role **(With 2 Managed Policies for S3 and CloudWatch)**
  - Instance Profile


# Prerequesites


1) Install eksctl on your EC2 instance.

    ` curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp`

    `sudo mv /tmp/eksctl /usr/local/bin`

2) Install kubectl on your EC2 instance.

    `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"`

    `sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl`
# Implementation Guide

## 1) Provision an EKS Cluster

  i. Provision an EKS cluster with three worker nodes in us-east-1:

  ```bash
  eksctl create cluster --name dev --version 1.16 --region us-east-1 --zones us-east-1a,us-east-1b --nodegroup-name standard-workers --node-type t3.micro --nodes 3 --nodes-min 1 --nodes-max 4 --managed
  ```

  Example response:

   ![result1](result1.png)

  ii. Creates a new cloudformation:

   ![result2](result2.png)

  iii. Enable it to connect to our cluster:

  `aws eks update-kubeconfig --name dev --region us-east-1`

  Result:

  ![allow-connection](allow-connection.png)
## 2) Create a Deployment on Your EKS Cluster

1) Create the service:

    `kubectl apply -f ./svc.yaml`


2) Check its status:

    `kubectl get service`

    Example response:

    ![get-service](get-service.png)

    Copy the external IP of the load balancer, and paste it into a text file, as we'll need it in a minute.

3) Create the deployment:

    `kubectl apply -f ./hello-deployment.yaml`

4) Check its status

    `kubectl get deployment`

    ![get-depl](get-depl.png)

5) View the pods

    `kubectl get pod`

    ![pods](pods.png)

6) View the ReplicaSets:

    `kubectl get rs`

7) View the nodes:

    `kubectl get node`

Access the application using the load balancer, replacing <LOAD_BALANCER_EXTERNAL_IP> with the IP you copied earlier:

curl "<LOAD_BALANCER_EXTERNAL_IP>"

# Results

Succesful backup

![backup-result-db](backup-result-db.png)


Succesful restore SNS email

![succ-restore](succ-restore.png)
# References

- https://www.youtube.com/watch?v=4jy2FILK5R8
- https://github.com/awslabs/efs-backup/blob/master/deployment/efs-to-efs-backup.template
- https://docs.aws.amazon.com/efs/latest/ug/efs-mount-helper.html
- https://docs.aws.amazon.com/solutions/latest/efs-to-efs-backup/template.html
- https://docs.aws.amazon.com/solutions/latest/efs-to-efs-backup/restore.html
- https://github.com/awslabs/efs-backup

# Hints

[How do I resolve cluster creation errors in Amazon EKS?](https://aws.amazon.com/premiumsupport/knowledge-center/eks-cluster-creation-errors/)

[Kubernetes ImagePullBackOff error: what you need to know](https://www.tutorialworks.com/kubernetes-imagepullbackoff/)
