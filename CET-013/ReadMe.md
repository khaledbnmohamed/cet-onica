CET-013

# Task Summary

* Use Jenkins to Run runway build on changes of a Github Repo

![Schema](schema.png)


## :large_orange_diamond: Resources :
1) The 2 CloudFormation Stacks Resources
2) EC2 Instance that hosts Jenkins

# Make it Work !

## :large_blue_diamond: Implementaion Guide
1) [Create an EC2 Instance and install jenkins on it](https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/#Download%20and%20Install%20Jenkins)
2) Install needed plugins on Jenkins
3) Install git on your EC2 instance
4) Get the git path from your EC2 instance using command

     `whereis git`

     Example:
     `/usr/bin/git`
5) Add the git path you just copied to the **Global Tool Configuration** in the **Path to Git executable** field
6) Add EC2 Plugin and choose Install without restart
7) Create a Github Repo that contains your project

      [Runway Repo](https://github.com/khaledbnmohamed/runway`)
8) Add SSH credentials for Github
9) Create a new Job on jenkins
10)  Specifiy the Github repo for this job

   Example:
   ![sourcecode](sourcecode.png)

11) Check the build trigger for Github hooks

   Example:
   ![build-trigger](build-trigger.png)
11) [Add Github Credentials to Jenkins GLOBALLY](https://plugins.jenkins.io/github/)

**Hint: TOKEN created must contains these previleges in github**

![token-hints](token-hints.png)

12) [Add AWS Credentials to Jenkins](https://www.jenkins.io/doc/book/using/using-credentials/#:~:text=From%20the%20Jenkins%20home%20page,Add%20Credentials%20on%20the%20left.)
13) Specifiy this newely added AWS credentials to the Build configuration for the job created

      Example:
      ![build-env](build-env.png)

14) Install runway on the EC2 instance
      ```bash
      curl -L https://oni.ca/runway/2.0.0/linux -o runway
      chmod +x runway
      ```
    
15) Add Build script to deploy using runway installed
      ```bash
      export DEPLOY_ENVIRONMENT="dev"
      cd /var/lib/jenkins/workspace/build-runway-from-github
      ./runway deploy
      ```

 ## :large_orange_diamond: Result



  1) [Jenkins URL](http://3.238.145.118:8080/login?from=%2Fjob%2Fbuild-runway-from-github%2F)
  2) Instance Created

# References
[Jenkins on AWS](https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/)
[Setting up a CI/CD pipeline by integrating Jenkins with AWS CodeBuild and AWS CodeDeploy](https://aws.amazon.com/blogs/devops/setting-up-a-ci-cd-pipeline-by-integrating-jenkins-with-aws-codebuild-and-aws-codedeploy/)

[Blog Post](https://www.serverless.com/blog/node-rest-api-with-serverless-lambda-and-dynamodb)

[Create Jenkins Job and Clone Repository From GitHub](https://narenchejara.medium.com/create-jenkin-job-and-clone-project-from-git-b513804d3089#:~:text=Clone%20Project%20From%20Git,(repository)%20from%20the%20Github.&text=Create%20a%20new%20Jenkins%20job,installed%20in%20the%20Jenkins%20machine.)
# Hints
How to define dependicies in the runway stacks file?

[Blog Post](https://www.serverless.com/blog/node-rest-api-with-serverless-lambda-and-dynamodb)
