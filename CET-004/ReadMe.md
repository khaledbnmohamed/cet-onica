CET-004

# Task Summary

  - Create an EC2 instance which functions as a web server accesbile on the internet with a frontpage cloned  from an S3 bucket

# Diagram

![image info](diagaram.png)

# CloudFormation resources

  - EC2 instance
  - Security Group

# CloudFormation Paramters:




| Variable          | Type     | Default Value      | Description                                                 |
| ----------------- | -------- | ------------------ | ----------------------------------------------------------- |
| `HTMLIndexPrefix` | String   | `index.html`       | Index file prefix                                           |
| `S3bucket`        | string   | `khaled-04-bucket` | Bucket Name to download the index file from                 |
| `Instance type`   | Dropdown | ` t2.micro`        | EC2 instance type                                           |
| `KeyName`         | AWS::EC2::KeyPair::KeyName dropdown | `khaled_awad_key`                | The key pair that will be used to SSH the EC2 instance with |
| `Subnet`          | AWS::EC2::Subnet::Id | `-`                | Subnet to attach the EC2 instance to                        |
| `InstanceName`          | String | `WebInstance1`                |Instance name


