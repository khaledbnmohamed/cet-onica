CET-017

# Task Summary

  - Lambda Function using python encrypt the S3 bucket if not encrypted once the bucket is created or its configuration is changed.

# Diagram

![image info](schema.png)

# CloudFormation resources

  - Lambda Function
  - IAM Role for Lambda Exceution
  - Custom Config Rule
  - Permission To Invoke Lambda


# Test it out!

  - Create a new bucket without SSE or remove Encryption from already created bucket
# Results

![result](result.png)

Bucket encryption changes

![result-2](result-2.png)
# References

[Automating AWS Lambda to Run Python For S3 Encryption](https://quileswest.medium.com/automating-aws-lambda-to-run-python-for-s3-encryption-559f624bf291)
