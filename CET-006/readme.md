CET-006

# Task Summary



Create an IAM User with certian policy

Create a new IAM user (programming access) access to :

​	1) List all buckets.

​	2) Full access to one of your buckets.

​	3 ) List all EC2.

​	4 ) Add its configuration(access and secret token) to machine

1) Create IAM User
   ```bash
    aws iam  create-user --user-name "khaled_6_user"
    ```

    Example response:

    ```bash
    {
        "User": {
            "Path": "/",
            "UserName": "khaled_6_user",
            "UserId": "AIDAYC572D54T74WQIP6T",
            "Arn": "arn:aws:iam::556063661945:user/khaled_6_user",
            "CreateDate": "2021-05-28T15:58:29+00:00"
        }
    }
    ```

2) Add Secret key and password to this user
    ```bash
    aws iam create-access-key --user-name "khaled_6_user"
    ```
    Example response:

    ```bash
        {
            "AccessKey": {
                "UserName": "khaled_6_user",
                "AccessKeyId": "AKIAYC572D54TED4PJEQ",
                "Status": "Active",
                "SecretAccessKey": "S7aNRti+NEEVUvtqgVPU******",
                "CreateDate": "2021-05-28T16:02:04+00:00"
            }
        }
    ```
3) Create IAM policy

    ```bash
    aws iam create-policy --policy-name "cet-06-khaled-list-buckets" --policy-document  file:///mnt/k/cet-onica/CET-006/CET-06-policy.json
    ```
4) Determine the Amazon Resource Name (ARN) of the policy to attach. The following command uses list-policies to find the ARN of the policy with the name PowerUserAccess. It then stores that ARN in an environment variable.

   ```bash
    export POLICYARN=$(aws iam list-policies --query 'Policies[?PolicyName==`cet-06-khaled-list-buckets`].Arn' --output text)

    aws iam attach-user-policy --user-name "khaled_6_user" --policy-arn "$POLICYARN"
   ```
5) Add the credentials to CET-Test profile in the credentials file in local machine

        vim ~/.aws/credentials

6) Verify that the policy is attached to the user by running the list-attached-user-policies command.
    ```bash
    aws iam list-attached-user-policies --user-name "cet-06-khaled-list-buckets"
    ```
    ```bash
    aws s3 ls --profile cet-test
    ```
    ```bash
    aws s3 ls khaled-04-bucket --profile cet-test
    ```
    Example response
    ```bash
    2021-05-28 18:13:05      29731 resume_khaled_awad.pdf
    ```
