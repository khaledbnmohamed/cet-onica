CET-005

# Task Summary
  - Create a VPC with public and private subnets using CF
  - Create EC2 instances in each subnet using CF
  - Connect to Public instances through the internet
  - Connect to the private instances throught the bastion host created in the public subnet (SSH tunneling)

# VPC CloudFormation Diagram
![image info](vpc-diagram.png)

# VPC CloudFormation Parameters
|Variable|Type|Default Value|Description|
|--------|-------------|---------|-------|
|`EnvironmentName`|string|`-`| An environment name that is prefixed to resource names |
|`VpcCIDR`|string|`10.192.0.0/16`| the IP range (CIDR notation) for this VPC |
|`PublicSubnet1CIDR`|string|`10.192.10.0/24`|IP range (CIDR notation) for the public subnet in the first Availability Zone|
|`PublicSubnet2CIDR`|string|`10.192.11.0/24`|IP range (CIDR notation) for the public subnet in the second Availability Zone|
|`PrivateSubnet1CIDR`|string|`10.192.20.0/24`|IP range (CIDR notation) for the private subnet in the first Availability Zone|
|`PrivateSubnet2CIDR`|string|`10.192.21.0/24`|IP range (CIDR notation) for the private subnet in the second Availability Zone|

# VPC CloudFormation resources
- VPC

- 2 Public Subnets

- 2 Private Subnets

- 2 ElasticIPs

- 2 NatGateways

- 2 RouteTables

- 2 Routes

- NACL

- *NACL Related* NACL-Subnet Assosciation and NACL entries

- 2 PublicSubnetRouteTableAssociations

- 2 PrivateSubnetRouteTableAssociations

- InternetGatewayAttachment
# Bastion Host connection Diagram *from the internet
![image info](ec2-diagram-2.png)

# EC2 CloudFormation Diagram
![image info](ec2-diagram.png)

# EC2 instances CloudFormation Parameters
|Variable|Type|Default Value|Description|
|--------|-------------|---------|-------|
|`InstanceType`|string|`t2.small`| InstanceType |
|`BastionHostWhitelistedIP`|string|`197.246.72.128/32`| the IP Used to access the bastion host |
|`KeyName`|AWS::EC2::KeyPair::KeyName|`khaled_awad_key`|Name of an existing EC2 KeyPair to enable SSH access to the instance|
|`NetworkStackNameParameter`|string|`CET-005-Khaled-VPC`| An environment name that is prefixed to resource names|

# EC2 instances CloudFormation resources
- 2 PublicWebServerInstances

- BastionHostInstance

- 2 PrivateWebServerInstance1

- BastionHostSecurityGroup

- PrivateInstanceSecurityGroup

- Public InstanceSecurityGroup


# Steps to create SSH tunnel to connect to the private instance:

Reference:
https://towardsdatascience.com/connecting-to-an-ec2-instance-in-a-private-subnet-on-aws-38a3b86f58fb

1) Start SSH Agent to start adding the key to the port forwarinfg option

``` bash
eval `ssh-agent -s`
ssh-add
```

2) Add the keys to your keychain
``` bash
ssh-add -k khaled_key_ohio.pem
```

3)  Check whether the private key is properly added to the key chain

``` bash
ssh-add -L
```

4) Use the config file for creating port forwarding from the bastion host to the private instance
 in your directory ssh

  type :
``` bash
vim ~/.ssh/config
```

  and add this configurations
```
Host bastion-instance
   HostName 3.83.132.237 #host public ip
   User ec2-user #ec2 user
Host private-instance
   HostName 10.192.20.22 #private ec2 ip
   User ec2-user #ec2 user
   ProxyCommand ssh -q -W %h:%p bastion-instance
```

5)  Connect to the private instance directly with

``` bash
ssh private-instance
```

# Task Notes

1) Avoid connecting to the private instance by adding the private key to the bastion host as it's a bad practice and security concern
