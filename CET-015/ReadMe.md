CET-015

# Build Docker Image and push it to ECR
# Task Summary

1) Create a Docker image that has Nginx installed from base centos image on your local machine.
2) Push your image to ECR service.
3) Create an ECS cluster of only one EC2 and a task definition using your created image to show a simple welcome HTML page.
   
   ![schema](schema.png)



## Prerequisites

  1) AWS CLI<a name="cli-install"></a>

<details>
<summary > 2. Docker </summary>

**To install Docker on an Amazon EC2 instance**

1. Launch an instance with the Amazon Linux 2 AMI\. For more information, see [Launching an Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/launching-instance.html) in the *Amazon EC2 User Guide for Linux Instances*\.

2. Connect to your instance\. For more information, see [Connect to Your Linux Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstances.html) in the *Amazon EC2 User Guide for Linux Instances*\.

3. Update the installed packages and package cache on your instance\.

   ```
   sudo yum update -y
   ```

4. Install the most recent Docker Community Edition package\.

   ```
   sudo amazon-linux-extras install docker
   ```

5. Start the Docker service\.

   ```
   sudo service docker start
   ```

6. Add the `ec2-user` to the `docker` group so you can execute Docker commands without using `sudo`\.

   ```
   sudo usermod -a -G docker ec2-user
   ```

7. Log out and log back in again to pick up the new `docker` group permissions\. You can accomplish this by closing your current SSH terminal window and reconnecting to your instance in a new one\. Your new SSH session will have the appropriate `docker` group permissions\.

8. Verify that the `ec2-user` can run Docker commands without `sudo`\.

   ```
   docker info
   ```
</details>

# Impelementation Guide




 ## A) Create Image and Push it to ECR


### Step 1: Create a Docker image<a name="cli-create-image"></a>

In this section, you create a Docker image of a simple web application, and test it on your local system or EC2 instance, and then push the image to a container registry \(such as Amazon ECR or Docker Hub\) so you can use it in an ECS task definition\.

**To create a Docker image of a simple web application**

1. Create a file called `Dockerfile`\. A Dockerfile is a manifest that describes the base image to use for your Docker image and what you want installed and running on it\. For more information about Dockerfiles, go to the [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)\.

   ```
   touch Dockerfile
   ```

2. Add the `Dockerfile` in the repo



3. <a name="sample-docker-build-step"></a>Build the Docker image from your Dockerfile\.


   ```
   docker build -t hello-world .
   ```

4. Run the newly built image\. The `-p 80:80` option maps the exposed port 80 on the container to port 80 on the host system\. For more information about docker run, go to the [Docker run reference](https://docs.docker.com/engine/reference/run/)\.

   ```
   docker run -t -i -p 80:80 hello-world
   ```


### Step 2: Authenticate to your default registry<a name="cli-authenticate-registry"></a>

+ [get\-login\-password](https://docs.aws.amazon.com/cli/latest/reference/ecr/get-login-password.html) \(AWS CLI\)

  ```
  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 556063661945.dkr.ecr.us-east-1.amazonaws.com
  ```


### Step 3: Create a repository<a name="cli-create-repository"></a>

Now that you have an image to push to Amazon ECR, you must create a repository to hold it\. In this example, you create a repository called `hello-world` to which you later push the `hello-world:latest` image\. To create a repository, run the following command:

```
aws ecr create-repository \
    --repository-name hello-world \
    --image-scanning-configuration scanOnPush=true \
    --region us-east-1
```

### Step 4: Push an image to Amazon ECR<a name="cli-push-image"></a>

1. List the images you have stored locally to identify the image to tag and push\.

   ```
   docker images
   ```

   Output:

   ```
   REPOSITORY          TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
   hello-world         latest              e9ffedc8c286        4 minutes ago       241MB
   ```

2. Tag the image to push to your repository\.

   ```
   docker tag hello-world:latest 556063661945.dkr.ecr.us-east-1.amazonaws.com/hello-world:latest
   ```

3. Push the image\.

   ```
   docker push 556063661945.dkr.ecr.us-east-1.amazonaws.com/hello-world:latest
   ```

   Output:

   ```
   The push refers to a repository [556063661945.dkr.ecr.us-east-1.amazonaws.com/hello-world] (len: 1)
   e9ae3c220b23: Pushed
   a6785352b25c: Pushed
   0998bf8fb9e9: Pushed
   0a85502c06c9: Pushed
   latest: digest: sha256:215d7e4121b30157d8839e81c4e0912606fca105775bb0636b95aed25f52c89b size: 6774
   ```

 ## B) Create ECS

 1) Create New Cluster
 2) Add our newely created image in

    ![using-image](using-image.png)

# References
* [Using Amazon ECR with the AWS CLI](https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html)
