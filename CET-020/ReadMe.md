CET-020

# Task Summary

  - Backup an EFS to EFS and being able to restore it when needed

# Diagram

![image info](schema.png)


# Workflow
• The orchestrator lambda function is first invoked by CW event (start backup) schedule defined by the customer. The lambda function creates a 'Stop Backup' CWE event and add the orchestrator (itself) lambda function as the target. It also updates desired capacity of the autoscaling group (ASG) to 1 (one). Auto Scaling Group (ASG) launches an EC2 instance that mounts the source and target EFS and backup the primary EFS.

• The orchestrator lambda function writes backup metadata to the DDB table with backup id as the primary key.

• Before the backup window defined by the customer, the 'Stop' CWE invokes orchestrator lambda to change the desired capacity of ASG to 0 (zero).

• The lifecycle hook CWE is triggered by ASG event (EC2_Instance_Terminating). This CWE invokes the orchestrator lambda function that use ‘AWS-RunShellScript’ document name to make send_command api call to the SSM service.

• During the lifecycle hook event, the EC2 instance will stop/cleanup rsync process gracefully and update the DDB table with the KPIs, upload logs to the S3 bucket.

• The EC2 successful termination trigger another lifecycle hook event. This event triggers the orchestrator lambda function to send the anonymous metrics, notify customer if complete backup was not done.

# Implementation Guide

## 1) Mount EFS to EC2 instance
  1) Install helper

      `sudo yum install -y amazon-efs-utils`


      `sudo mkdir /efs`

  2) Allow NFS connection to EFS for the security group of the EC2 instance


  3) Mount the EFS

      `sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport 172.31.2.171:/ efs`

  4) **Allow the NFS of the Main EFS security group to be accessed by the backup EFS security group, so the EC2 instance created can mount the main EFS**

      ![hint1](hint1.png)

## 2) Restore EFS CloudFormation

  1) Use the source EFS = Main EFS

  2) Use Desitnation EFS = Backup EFS

  3) Security Group = Backup EFS security Group





# Results

Succesful backup

![backup-result-db](backup-result-db.png)


Succesful restore SNS email

![succ-restore](succ-restore.png)
# References

- https://www.youtube.com/watch?v=4jy2FILK5R8
- https://github.com/awslabs/efs-backup/blob/master/deployment/efs-to-efs-backup.template
- https://docs.aws.amazon.com/efs/latest/ug/efs-mount-helper.html
- https://docs.aws.amazon.com/solutions/latest/efs-to-efs-backup/template.html
- https://docs.aws.amazon.com/solutions/latest/efs-to-efs-backup/restore.html
- https://github.com/awslabs/efs-backup

