CET-008

# Temporary Cross account access
# Task Summary

1) Creating a bucket and sync folder using CLI.


2) Create a cross-role to be accessed by another account:

   ## Main Users : Users who have direct access to S3 bucket
   ## Outsider Users : Users who will use Main users access to access the s3 bucket in their(Main) account
   ![cross role](cross-role.png)
      a)   Create a cross-account role in your onica-sso lab with the privilege to just list all buckets and provide the going infinity account number in the trusted entities.

      b)  Assume the role you created with provided credentials using AWS CLI.


# Impelementation Guide


1) Create s3 bucket


   ```bash
   aws s3 create-bucket --acl private --bucket cet-08-khaled-bucket --region us-east-1
   ```


2)  Sync bucket from folder
      ```bash
      aws s3 sync . s3://cet-08-khaled-bucket
      ```


3) Create **role** with the trust policy **(root account in the Main account which have access to s3 and will be used by other users to access s3 in this account)**

    ```bash
    aws iam create-role --role-name test-role --assume-role-policy-document  file:///mnt/k/cet-onica/CET-008/trust-policy.json
    ```

   Example respone : ![](trust-role.png)
   with ARN : `arn:aws:iam::556063661945:role/test-role`

4) Attach List S3 Buckets policy to the created role

   ```bash
   aws iam attach-role-policy --role-name test-role --policy-arn "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
   ```
5) Verify by listing role policies

   ```bash
   aws iam list-attached-role-policies --role-name test-role
   ```
   Example response:

   ![](attached-role.png)

6) Add Outsider user configuration to the aws credentials file using
   ```bash
   vim ~/.aws/credentials
   ```
7)  Verify having the Outsider user credentials in our CLI configuration by the command
   ```bash
   aws sts get-caller-identity --profile going
   ```
   Example response:
   ![](get-caller.png)
## B ) :red_circle: Start the REAL WORK HERE  by start assuming role
<br>

   1) Use assume role command

      ```bash
      export ROLEARN=$(aws iam list-roles --query "Roles[?RoleName == 'test-role'].[Arn]" --output text)

      export RESULT=$(aws sts assume-role --role-arn "$ROLEARN" --role-session-name AWSCLI-Session --profile going)
      ```

   2) Store the tokens in Env Variables

      ```bash
      export AWS_ACCESS_KEY_ID=$(echo $RESULT | jq -r '.Credentials' | jq -r '.AccessKeyId')
      export AWS_SECRET_ACCESS_KEY=$(echo $RESULT | jq -r '.Credentials' | jq -r '.SecretAccessKey')
      export AWS_SESSION_TOKEN=$(echo $RESULT | jq -r '.Credentials' | jq -r '.SessionToken')
      ```

   3) Check Current set user with

      ```bash
      aws sts get-caller-identity
      ```
   4)  Try listing the S3 buckets with cross account
         ```bash
         aws s3 ls
          ```

          Example success response:

          ![Success image](success.png)

   4)  Try getting certain account
         ```bash
         aws s3 ls cet-08-khaled-bucket
          ```

          Example success response:

          ![Success image](success.png)

   1) Finally unset the env varaiables to clean up
   ```bash
   unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN
   ```

<br>

# References
* [Youtube Video: AWS - IAM Cross Account Access
](https://www.youtube.com/watch?v=C2jJ8ZvDkL0)
