CET-010

# Task Summary

  - Using Terraform, Create an EC2 instance which functions as a web server and connect to s3 bucket.
  - Deployment is triggered on S3 bucket update

# Diagram

![image info](schema.png)

# Terraform Resources

  - EC2 instance
  - S3 Bucket
  - CodeDeploy App
  - Deployment Group
  - SNS Topic
  - SNS Subscription
  - IAM Roles
  - Security Group

# Terraform Parameters:




| Variable          | Type     | Default Value      | Description                                                 |
| ----------------- | -------- | ------------------ | ----------------------------------------------------------- |
| `instance_name`          | String | `ExampleAppServerInstance`                |Instance name
| `codedeploy_application_name` | String   | `example-app`       | CodeDeploy Application name.|
| `bucket_name`        | string   | `app-codedeploy-releases` | Bucket Name to download the index file from                 |
| `Instance_type`   | string | ` t2.micro`        | EC2 instance type                                           |
| `key_pair`         | string | `khaled_key`                | The key pair that will be used to SSH the EC2 instance with |
| `subscription_email`          | Value of email used for subscirption | `khaled.awad@rackspace.com`                | Email                       |


# Implementation Guide:

1) `terraform apply --auto-approve`

   Example on success:

   ![](apply.png)
2) Upload the zip file that wil be used in the EC2 instance
