import boto3, urllib, os
from base64 import b64decode
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
client = boto3.client('codedeploy')

def lambda_handler(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name'];
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    # SET AS ENV VARIABLES
    response = client.create_deployment(
        applicationName= os.environ.get('APPLICATION_NAME'),
        deploymentGroupName= os.environ.get('DEPLOYEMNT_GROUP_NAME'),
        description='string',
        ignoreApplicationStopFailures=False,
        autoRollbackConfiguration={
            'enabled': True,
            'events': [
                'DEPLOYMENT_FAILURE',
            ]
        },
        revision={
        'revisionType': 'S3',
        's3Location': {
            'bucket': bucket,
            'key': key,
            'bundleType': 'zip',
        },
        },
        updateOutdatedInstancesOnly=False
        )
    print(response)
