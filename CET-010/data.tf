data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

# data "archive_file" "ingest_function_archive" {
#    type        = "zip"
#    source_dir  = "${path.module}/code"
#    output_path = "${path.module}/code/ingest_function.zip"
#  }

data "template_file" "lambda_policy_file" {
  template = file("policies/inline_lambda_policy.json.tpl")

  vars = {
    region     = "${data.aws_region.current.name}",
    account_id = "${data.aws_caller_identity.current.account_id}",
    app_name   = "${aws_codedeploy_app.example.name}"
  }

}
