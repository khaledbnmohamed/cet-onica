terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

# Need to re-check
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

locals {
  lambda_zip_file = "lambda-deploy"
  function_name   = "lambda_handler"
}

resource "aws_security_group" "allow_http" {
  name = "allow_http"

  ingress {
    description = "Http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

# Eq to CodeDeploy Trust json
resource "aws_iam_role" "deployment_group_role" {
  name = "CET-10-DeployemntGroup-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}



# Add all json in one file
resource "aws_iam_policy" "policy_one" {
  name = "read-s3-bucket"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["s3:Get*"]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

# Eq to EC2 role
resource "aws_iam_role" "ec2-role" {
  name = "CET-10-EC2-role"

  assume_role_policy  = data.aws_iam_policy_document.instance-assume-role-policy.json
  managed_policy_arns = [aws_iam_policy.policy_one.arn]

}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "CET-10-profile"
  role = aws_iam_role.ec2-role.name
}

resource "aws_instance" "app_server" {
  ami                  = "ami-0d5eff06f840b45e9"
  instance_type        = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  key_name             = var.key_pair
  user_data            = file("install-codedeploy.sh")
  security_groups      = [aws_security_group.allow_http.name]

  tags = {
    Name = "${var.instance_name}"
  }
}

# eq to Attach role to a CodeDeploy Policy for EC2 usage
resource "aws_iam_role_policy_attachment" "AWSCodeDeployRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
  role       = aws_iam_role.deployment_group_role.name
}

resource "aws_codedeploy_app" "example" {
  name = var.codedeploy_application_name
}

resource "aws_sns_topic" "example" {
  name   = "s3-event-notification-topic"
  policy = <<POLICY
{
    "Version":"2012-10-17",
    "Statement":[{
        "Effect": "Allow",
        "Principal": { "Service": "s3.amazonaws.com" },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:*:*:s3-event-notification-topic",
        "Condition":{
            "ArnLike":{"aws:SourceArn":"${module.codedeploy_bucket.bucket_arn}"}
        }
    }]
}
POLICY
}

resource "aws_sns_topic_subscription" "invoke_with_sns" {
  topic_arn = aws_sns_topic.example.arn
  protocol  = "email"
  endpoint  = var.subscription_email
  // TODO use cloudformation, **try** to use emails  as array

}



# Create Deployemnt group
resource "aws_codedeploy_deployment_group" "example" {
  app_name              = aws_codedeploy_app.example.name
  deployment_group_name = "Deployemnt-Group-Terraform"
  service_role_arn      = aws_iam_role.deployment_group_role.arn

  ec2_tag_set {
    ec2_tag_filter {
      key   = "Name"
      type  = "KEY_AND_VALUE"
      value = "ExampleAppServerInstance"
    }

  }

  trigger_configuration {
    trigger_events     = ["DeploymentStart"]
    trigger_name       = "Failure Trigger"
    trigger_target_arn = aws_sns_topic.example.arn
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  alarm_configuration {
    alarms  = ["my-alarm-name"]
    enabled = true
  }
}

module "codedeploy_bucket" {
  source      = "./s3bucket"
  bucket_name = var.bucket_name
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  inline_policy {
    name   = "my_inline_policy"
    policy = data.template_file.lambda_policy_file.rendered
  }

}

resource "aws_lambda_permission" "allow_bucket1" {
  statement_id  = "AllowExecutionFromS3Bucket1"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.func1.arn
  principal     = "s3.amazonaws.com"
  source_arn    = module.codedeploy_bucket.bucket_arn
}

resource "aws_lambda_function" "func1" {
  filename         = "${local.lambda_zip_file}.zip"
  function_name    = local.function_name
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = "${local.lambda_zip_file}.lambda_handler"
  source_code_hash = filebase64sha256("${local.lambda_zip_file}.zip")
  runtime          = "python3.8"

  environment {
    variables = {
      APPLICATION_NAME      = aws_codedeploy_app.example.name,
      DEPLOYEMNT_GROUP_NAME = aws_codedeploy_deployment_group.example.deployment_group_name
    }
  }
}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = module.codedeploy_bucket.bucket_id

  lambda_function {
    lambda_function_arn = aws_lambda_function.func1.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [
    aws_lambda_permission.allow_bucket1,
  ]
}
# TODO many email subscription using terraform functions
