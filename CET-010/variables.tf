variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}

variable "instance_type" {
  description = "Value of instance type"
  type        = string
  default     = "t2.micro"
}

variable "bucket_name" {
  description = "Value of the Name tag for the S3 bucket"
  type        = string
  default     = "app-codedeploy-releases"
}

variable "key_pair" {
  description = "Value of the key pair"
  type        = string
  default     = "khaled_key"
}

variable "subscription_email" {
  description = "Value of email"
  type        = string
  default     = "khaled.awad@rackspace.com"
}

variable "codedeploy_application_name" {
  description = "Value of code deploy app name"
  type        = string
  default     = "example-app"
}


