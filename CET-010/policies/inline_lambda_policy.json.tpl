{
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "codedeploy:CreateDeployment"
          ],
          "Resource" : [
            "arn:aws:codedeploy:${region}:${account_id}:deploymentgroup:${app_name}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "codedeploy:GetDeploymentConfig"
          ],
          "Resource" : [
            "arn:aws:codedeploy:${region}:${account_id}:deploymentconfig:*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "codedeploy:GetApplicationRevision"
          ],
          "Resource" : [
            "arn:aws:codedeploy:${region}:${account_id}:application:${app_name}"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "codedeploy:RegisterApplicationRevision"
          ],
          "Resource" : [
            "arn:aws:codedeploy:${region}:${account_id}:application:${app_name}"
          ]
        }
      ]
    }
