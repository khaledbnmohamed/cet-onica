CET-014  Auto Scaling group with Terraform


# Task Summary

* create an Application load balancer to distribute the traffic across web servers created by Auto-scaling group.

![Schema](schema.jpeg)

## :large_blue_diamond: Modules :
1) Networking
      #### Create the Base VPC with 2 Public and 2 Private Subnets


      ## Inputs

      | Name | Description | Type | Default | Required |
      |------|-------------|------|---------|:--------:|
      | environment | The Deployment environment | `any` | n/a | yes |
      | vpc_cidr |The CIDR block of the vpc | `string` | n/a | yes |
      | public_subnets_cidr | The CIDR block for the public subnet packages | `list` | ` n/a` | no |
      | private_subnets_cidr | The CIDR block for the private  subnet packages | `list` | ` n/a` | no |
      | region | The region  subnet packages | `list` | ` n/a` | no |
      |availability_zones  | The az that the resources will be launched | `list` | ` n/a` | no |
      ## Outputs

      | Name | Description |
      |------|-------------|
      | vpc_id | n/a |
      | public_subnets_id | n/a |
      | private_subnets_id | n/a |
2) AutoScaling
      #### Create the autoscaling group with its instance and configuraiton

      ## Inputs

      | Name | Description | Type | Default | Required |
      |------|-------------|------|---------|:--------:|
      | image_id | The Image ID | `string` | `ami-084fa239e6e018cf7` | yes |
      | key_pair | The key_pair | `string` |`khaled_key` | yes |
      | networking_module_private_subnets |The networking_module_private_subnets | `list` | n/a | yes |

      ## Outputs

      | Name | Description |
      |------|-------------|
      | aws_autoscaling_group_id | n/a |
      | aws_autoscaling_group | n/a |

2) LoadBalancing
      #### Create the LoadBalancer, Target Groups and match them together

      ## Inputs

      | Name | Description | Type | Default | Required |
      |------|-------------|------|---------|:--------:|
      | networking_module_public_subnets" | The public subnets | `list` | `module.networking.public_subnets_id` | yes |
      | vpc_id | The current project | `string` | `module.networking.vpc_id` | yes |
      | aws_autoscaling_group_id | autoscaling id | `string` | `module.autoscaling.aws_autoscaling_group_id` | yes |

## :large_orange_diamond: Resources :
1) VPC
2) 2 Public and 2 Privates subnet
3) Internet gateways, RouteTables, Route, Security group
4) AutoScaling group
5) Application Load Balancer along with Target Group
6) Base of 3 EC2 Instances (created and managed by AutoScaling group) with Min of 2 and Max of 6 all in private subnets

# Make it Work !

## :large_blue_diamond: Implementaion Guide
1) using `terraform apply`
 ## :large_orange_diamond: Result



  1) [Application Load Balancer DNS](cet-014-alb-506735879.us-east-1.elb.amazonaws.com)


![result](result.png)
