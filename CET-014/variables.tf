variable "environment" {
  description = "The Deployment environment"
  default="dev"
}

variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
  default= "10.192.0.0/16"
}

variable "public_subnets_cidr" {
  type        = list
  description = "The CIDR block for the public subnet"
  default= ["10.192.10.0/24","10.192.11.0/24"]
}

variable "private_subnets_cidr" {
  type        = list
  description = "The CIDR block for the private subnet"
  default= ["10.192.20.0/24", "10.192.21.0/24"]
}

variable "region" {
  description = "The region to launch the bastion host"
  default= "us-east-1"

}

variable "availability_zones" {
  type        = list
  description = "The az that the resources will be launched"
  default = ["us-east-1a","us-east-1b"]
}
