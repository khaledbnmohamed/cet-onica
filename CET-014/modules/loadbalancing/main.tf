resource "aws_lb" "test" {
  name               = "cet-014-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = var.networking_module_public_subnets

  enable_deletion_protection = false

  # access_logs {
  #   bucket  = aws_s3_bucket.lb_logs.bucket
  #   prefix  = "test-lb"
  #   enabled = true
  # }

  tags = {
    Name = "CET-014"
  }
}
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.test.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group_1.arn
  }
}
resource "aws_lb_target_group" "target_group_1" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = var.aws_autoscaling_group_id
  alb_target_group_arn   = aws_lb_target_group.target_group_1.arn
}
