resource "aws_launch_configuration" "as_conf" {
  name_prefix   = "terraform-lc-example-"
  image_id      = var.image_id
  instance_type = "t2.micro"
  key_name      = var.key_pair
  user_data     = file("${path.module}/instance_user_data.sh")
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "auto_scale_group_1" {
  name                 = "terraform-asg-example"
  launch_configuration = aws_launch_configuration.as_conf.name
  min_size             = 2
  max_size             = 6
  vpc_zone_identifier  = var.networking_module_private_subnets

  lifecycle {
    create_before_destroy = true
    ignore_changes = [load_balancers, target_group_arns]
  }

    tags = [
    {
      key                 = "application"
      value               = "example"
      propagate_at_launch = true
    }
  ]

}

resource "aws_autoscalingplans_scaling_plan" "example" {
  name = "example-dynamic-cost-optimization"

  application_source {
    tag_filter {
      key    = "application"
      values = ["example"]
    }
  }

  scaling_instruction {
    max_capacity       = 6
    min_capacity       = 3
    resource_id        = format("autoScalingGroup/%s", aws_autoscaling_group.auto_scale_group_1.name)
    scalable_dimension = "autoscaling:autoScalingGroup:DesiredCapacity"
    service_namespace  = "autoscaling"

    target_tracking_configuration {
      predefined_scaling_metric_specification {
        predefined_scaling_metric_type = "ASGAverageCPUUtilization"
      }
      scale_in_cooldown = 60
      target_value = 50
    }
  }
}
