variable "image_id" {
  description = "The Image ID"
  default="ami-084fa239e6e018cf7"
}

variable "key_pair" {
  description = "The key_pair"
  default="khaled_key"
}

variable "networking_module_private_subnets" {
  description = "The networking_module_private_subnets"
}
