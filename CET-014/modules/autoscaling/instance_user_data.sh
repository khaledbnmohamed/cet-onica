#!/bin/bash
yum update -y
yum install httpd
cd /var/www/html
instance_id=$(curl -X GET http://169.254.169.254/latest/meta-data/instance-id)
random=$(echo "#$(openssl rand -hex 3)")
cat << 'EOF' > index.html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <style>
    body {
      color: #23das;
      background-color: {{random}};
      font-family: Arial, sans-serif;
      font-size:14px;
    }
  </style>
</head>
<body>
  <div align="center"><h1>This Instance Number is {{instance_ip}} </h1></div>
  <div>

  <img src="https://www.stickerstelegram.com/wp-content/uploads/2015/10/cat1.png"
   style="width: 700; height: 900px; margin: 0px; display: flex; margin-left: auto;
   margin-right: auto;">
   </div>
</body>
</html>
EOF
sed -i "s/{{instance_ip}}/$instance_id/"  index.html
sed -i "s/{{random}}/$random/"  index.html

chkconfig httpd on
service httpd start
