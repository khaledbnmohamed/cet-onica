output "aws_autoscaling_group" {
  value = "${aws_autoscaling_group.auto_scale_group_1.name}"
}

output "aws_autoscaling_group_id" {
  value = "${aws_autoscaling_group.auto_scale_group_1.id}"
}
