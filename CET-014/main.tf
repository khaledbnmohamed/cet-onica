resource "random_id" "random_id_prefix" {
  byte_length = 2
}
/*====
Variables used across all modules
======*/
locals {
  production_availability_zones = ["${var.region}a", "${var.region}b", "${var.region}c"]
}

module "networking" {
  source = "./modules/networking"

  region               = var.region
  environment          = var.environment
  vpc_cidr             = var.vpc_cidr
  public_subnets_cidr  = var.public_subnets_cidr
  private_subnets_cidr = var.private_subnets_cidr
  availability_zones   = local.production_availability_zones
}

module "autoscaling" {
  source = "./modules/autoscaling"
  networking_module_private_subnets = module.networking.private_subnets_id
}

module "loadbalancing" {
  source = "./modules/loadbalancing"
  networking_module_public_subnets = module.networking.public_subnets_id
  aws_autoscaling_group_id = module.autoscaling.aws_autoscaling_group_id
  vpc_id =  module.networking.vpc_id
}
