CET-019

# Task Summary

  - Create an EC2 instance which functions as a web server accesbile on the internet with a frontpage cloned  from an S3 bucket
  - **Monitor the server logs using CloudWatch**

# Diagram

![image info](diagaram.png)

# CloudFormation resources

  - EC2 instance
  - Security Group
  - IAM Role **(With 2 Managed Policies for S3 and CloudWatch)**
  - Instance Profile


# What is new from CET-04

  - Attach an IAM role to send CloudWatch Agent metrics to CloudWatch

    ![change3](change3.png)

  - Install CloudWatch Agent to send logs to CloudWatch

    ![change2](change2.png)

  - Configure CloudWatch and add config file that write the data from httpd access logs (located at /var/log/httpd/access_log) to “httpd_log” log group and “access_log” log stream.

    ![change1](change1.png)

# CloudFormation Paramters:


| Variable          | Type     | Default Value      | Description                                                 |
| ----------------- | -------- | ------------------ | ----------------------------------------------------------- |
| `HTMLIndexPrefix` | String   | `index.html`       | Index file prefix                                           |
| `S3bucket`        | string   | `khaled-04-bucket` | Bucket Name to download the index file from                 |
| `Instance type`   | Dropdown | ` t2.micro`        | EC2 instance type                                           |
| `KeyName`         | AWS::EC2::KeyPair::KeyName dropdown | `khaled_awad_key`                | The key pair that will be used to SSH the EC2 instance with |
| `Subnet`          | AWS::EC2::Subnet::Id | `-`                | Subnet to attach the EC2 instance to                        |
| `InstanceName`          | String | `WebInstance1`                |Instance name


# Results

![result](result.png)


# References

[Installing and running the CloudWatch agent on your servers](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/install-CloudWatch-Agent-commandline-fleet.html)

[Create IAM roles to use with the CloudWatch agent on Amazon EC2 instances](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/create-iam-roles-for-cloudwatch-agent.html#create-iam-roles-for-cloudwatch-agent-roles)


# Hints

[Why I have to restart CloudWatch?](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/troubleshooting-CloudWatch-Agent.html#CloudWatch-Agent-troubleshooting-update-no-new-metrics)

