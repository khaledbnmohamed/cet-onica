CET-009

# Task Summary

* Using CodeDeploy to deploy changes from S3 bucket to a running server using **Windows Server EC2 Instance**

![Schema](schema.png)



# Make it Work !

## :large_orange_diamond: Resources :
1) Index file
2) A script that install server that runs on the before install life cycle
3) AppSpec file which used by CodeDeploy to include the index file and the script

## :large_blue_diamond: Service Role Creation
1) Create a **service role** for the CodeDeploy services to allow it to access AWS resources like S3 and attach CodeDeploy managed policy to it 

    ```bash
    aws iam create-role --role-name CodeDeployServiceRole --assume-role-policy-document file://CodeDeployDemo-Trust.json

    aws iam attach-role-policy --role-name CodeDeployServiceRole --policy-arn arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole
    ```

3) Create the EC2 Instance **Which a windows server in this task**
## :large_orange_diamond: IAM Instance Profile creation


1) Create **IAM Role**  and attach S3 permission to it
   ```bash
    aws iam create-role --role-name CodeDeployDemo-EC2-Instance-Profile --assume-role-policy-document file://CodeDeployDemo-EC2-Trust.json

    aws iam put-role-policy --role-name CodeDeployServiceRole --policy-name CodeDeployDemo-EC2-Permissions --policy-document file://CodeDeployDemo-EC2-Permissions.json
   ```


2) Call the attach-role-policy to give the role Amazon EC2 Systems Manager permissions so that SSM can install the CodeDeploy agent
      ```bash
    aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore --role-name CodeDeployDemo-EC2-Instance-Profile
   ```
3) Create Instance Profile and attach the role to it
   ```bash
   aws iam create-instance-profile --instance-profile-name CodeDeployDemo-EC2-Instance-Profile

   aws iam add-role-to-instance-profile --instance-profile-name CodeDeployDemo-EC2-Instance-Profile --role-name CodeDeployServiceRole
   ```



4) Call the create-application command to register a new application named HelloWorld_App with CodeDeploy
   ```bash
   aws deploy create-application --application-name HelloWorld_App
   ```
    ![app-success](app-success.png)

5) Get your service role ARN
   ```bash
    export SERVICEARN=$(aws iam get-role --role-name CodeDeployServiceRole --query "Role.Arn" --output text)
   ```

6)  Create Deployemnt group

    ```bash
    aws deploy create-deployment-group --application-name HelloWorld_App --deployment-group-name HelloWorld_DepGroup --deployment-config-name CodeDeployDefault.OneAtATime --ec2-tag-filters Key=Name,Value=CodeDeployDemo,Type=KEY_AND_VALUE --service-role-arn $SERVICEARN
    ```
    
    ![succ-group](succ-group.png)

## :large_orange_diamond: Deployment

7) Push changes
   ```bash
   aws deploy push --application-name HelloWorld_App --s3-location s3://khaled-04-bucket/archived.zip --ignore-hidden-files
   ```

8)   Create a deployemnt
        ```bash
        aws deploy create-deployment --application-name HelloWorld_App --s3-location bucket=khaled-04-bucket,key=archived.zip,bundleType=zip --deployment-group-name HelloWorld_DepGroup
        ```
        ![](deployemnt-created.png)
9)  Check deployment status

      ```bash
      aws deploy get-deployment --deployment-id d-RPSSBR88B --query "deploymentInfo.st
      atus" --output text
      ```
 ## :large_blue_diamond: Result
  Finally you should end up with this when you access your instance Public IP

 [Instance IP](http://35.175.129.164)
![final](final.png)
# References
* [Code Deploy fails without any error message](https://stackoverflow.com/questions/44574120/code-deploy-fails-without-any-error-message)


# Hints
* Make sure that the CodeDeploy Agent is running on your EC2 instance
[Make Sure CodeDeploy Agent is running](https://docs.aws.amazon.com/codedeploy/latest/userguide/codedeploy-agent-operations-install.html)
