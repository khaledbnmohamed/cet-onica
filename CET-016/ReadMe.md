CET-016


# Task Summary
  The completed pipeline detects changes to  image, which is stored in an image repository such as Amazon ECR, and uses CodeDeploy to route and deploy traffic to an Amazon ECS cluster and load balancer.
  ![schema](schema.png)

# Steps

  - [1) Create image and push to an Amazon ECR repository](#1-create-image-and-push-to-an-amazon-ecr-repository)
  - [2) Create task definition and AppSpec source files and push to a CodeCommit repository](#2-create-task-definition-and-appspec-source-files-and-push-to-a-codecommit-repository)
  - [3 ) Create  Application Load Balancer and target groups](#3--create--application-load-balancer-and-target-groups)
  - [4 )  Create  Amazon ECS cluster and service](#4--create--amazon-ecs-cluster-and-service)
  - [5 )  Create  CodeDeploy application and deployment group ](#5--create--codedeploy-application-and-deployment-group)
  - [6 )  Create  pipeline](#6--create--pipeline)
# Implementation Guide

## 1) Create image and push to an Amazon ECR repository
   Explained in CET-015
## 2) Create task definition and AppSpec source files and push to a CodeCommit repository
<details>
<summary >More Details </summary>
1) Register  task definition with the taskdef.json file

   ```bash
   aws ecs register-task-definition --cli-input-json file://taskdef.json
   ```

   Example respone:
   ![resp1](resp1.png)

</details>

## 3 ) Create Application Load Balancer and target groups
<details >
<summary> More details </summary>
   a. Add 2 Public Subents from differnet AZs

   b. Add 2 Targets groups one on port `80` and one on port `8080`
</details>

## 4 )  Create  Amazon ECS cluster and service
<details >
<summary> More details </summary>
   a.using Networking only cluster

   b. Run the create-service command, specifying the JSON file create-service

   `aws ecs create-service --service-name my-service --cli-input-json file://create-service.json`
</details>

## 5 )  Create  CodeDeploy application and deployment group
<details >
<summary> More details </summary>

   a. In Compute platform, choose **Amazon ECS**.

   b. In Service role, choose a service role that grants CodeDeploy access to Amazon ECS. To create a new service role

   c. From Load balancers, choose the name of the load balancer that serves traffic to  Amazon ECS service

   c. From Production listener port, choose the port and protocol for the listener that serves production traﬃc to  Amazon ECS service. From Test listener port, choose the port and protocol for the test listener.
   
   d. Choose Reroute traffic immediately to determine how long after a successful deployment to reroute traffic to  updated Amazon ECS task.
</details>


## 6 ) Create  pipeline

<details >
<summary> More details </summary>


   a. Add Action provider, choose Amazon ECR
   * From AWS Documentation
   ![add-ecr](add-ecr.png)

</details>

# Results

Deployement Done
![allow-traffic](allow-traffic.png)

Result in CloudWatch

![cloudwatch](cloudwatch.png)

# References
* https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-ecs-ecr-codedeploy.html#tutorials-ecs-ecr-codedeploy-update
