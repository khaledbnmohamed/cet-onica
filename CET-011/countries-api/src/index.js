const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const {
    routes: countriesRoute,
} = require('./countries/routes.js');

const {
  routes: countryRoutes,
} = require('./country/routes.js');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use('/countries', countriesRoute);
app.use('/country', countryRoutes);


module.exports = app;
