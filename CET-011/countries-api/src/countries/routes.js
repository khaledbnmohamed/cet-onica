const express = require('express');

const routes = express.Router({
    mergeParams: true
});

const AWS = require('aws-sdk');
AWS.config.update( {
  region: 'us-east-1'
});
const dynamodb = new AWS.DynamoDB.DocumentClient();
const dynamodbTableName = "countriesTable";

routes.get('/', async (req, res) => {
    const response = await getCountries()
    res.status(response.statusCode).json(response.body);
});

routes.post('/', async (req, res) => {
  const response = await saveCountry(req.body)
  res.status(response.statusCode).json(response.body);
});


async function getCountries() {
  const params = {
    TableName: dynamodbTableName
  }
  const allCountries = await scanDynamoRecords(params, []);
  const body = {
    countries: allCountries
  }
  return await buildResponse(200, body);
}

async function scanDynamoRecords(scanParams, itemArray) {
  try {
    const dynamoData = await dynamodb.scan(scanParams).promise();
    itemArray = itemArray.concat(dynamoData.Items);
    if (dynamoData.LastEvaluatedKey) {
      scanParams.ExclusiveStartkey = dynamoData.LastEvaluatedKey;
      return await scanDynamoRecords(scanParams, itemArray);
    }
    return itemArray;
  } catch(error) {
    console.error('Unexpected Error:', error);
  }
}


async function saveCountry(requestBody) {
  const params = {
    TableName: dynamodbTableName,
    Item: { id: requestBody.id,
      name:requestBody.name.toLowerCase()
    }
  }
  return await dynamodb.put(params).promise().then(() => {
    const body = {
      Operation: 'SAVE',
      Message: 'SUCCESS',
      Item: { id: requestBody.id,
              name: requestBody.name.toLowerCase()
            }
    }
    return buildResponse(200, body);
  }, (error) => {
    console.error('Unexpected Error:', error);
  })
}

function buildResponse(statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      'Content-Type': 'application/json'
    },
    body: body
  }
}


module.exports = {
    routes, saveCountry, buildResponse
};
