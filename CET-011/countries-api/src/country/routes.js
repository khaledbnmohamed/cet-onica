
const express = require('express');
const AWS = require('aws-sdk');
const routes = express.Router({ mergeParams: true });
const dynamodb = new AWS.DynamoDB.DocumentClient();
const dynamodbTableName = "countriesTable";
const { buildResponse } = require("../countries/routes.js")


var fs = require("fs");

AWS.config.update({region: "us-east-1"});

routes.post('/', async (req, res) => {
  const response = await seedCountries();
  res.status(200).json("Seeded");
});

routes.get('/', async (req, res) => {
  const response = await getCountry(req.query.name);
  res.status(response.statusCode).json(response.body);
});




async function getCountry(name) {
  const params = {
    TableName: dynamodbTableName,
    Key: {
      "name": name.toLowerCase()
    }
  }
  // Use id as key TODO, don't use scan method
  return await dynamodb.get(params).promise().then((response) => {
    if (response.Item){
    return buildResponse(200, response.Item)}
    else{
      return buildResponse(404,"Not Found")}

  }, (error) => {
    console.error('Unexpected Error:', error);
    return buildResponse(500, error);
  });
}



module.exports = {
  routes
};
