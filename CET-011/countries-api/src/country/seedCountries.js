  const AWS = require('aws-sdk');
  const axios = require("axios");
  const dynamodb = new AWS.DynamoDB.DocumentClient();
  const dynamodbTableName = "countriesTable";


  AWS.config.update({region: "us-east-1"});

  async function seedCountries(event, context) {
    if (event.RequestType === "Delete") {
      return sendResponse(event, context, "SUCCESS", {});
    }
  console.log("Loading countries into DynamoDB");
  var countries = [
    {
      "id": "1",
      "name" : "Egypt"
    },
    {
      "id": "2",
      "name" : "KSA"
    },
    {
      "id": "3",
      "name" : "USA"
    },
    {
      "id": "4",
      "name" : "Moroco"
    },
    {
      "id": "5",
      "name" : "Paletsine"
    }
    ]

  countries.forEach(async (country) => {
    const params = {
      TableName: dynamodbTableName,
      Item: country
    }
    return await dynamodb.put(params).promise().then(() => {
    }, (error) => {
      console.error('Unexpected Error:', error);
    })
  });
  await sendResponse(event, context, "SUCCESS", { value : "Success"});

  }

  const sendResponse = async (
    event,
    context,
    responseStatus,
    responseData,
    physicalResourceId
  ) => {
    const reason =
      responseStatus == "FAILED"
        ? "See the details in CloudWatch Log Stream: " + context.logStreamName
        : undefined;

    const responseBody = JSON.stringify({
      StackId: event.StackId,
      RequestId: event.RequestId,
      Status: responseStatus,
      Reason: reason,
      PhysicalResourceId: physicalResourceId || context.logStreamName,
      LogicalResourceId: event.LogicalResourceId,
      Data: responseData
    });

    const responseOptions = {
      headers: {
        "Content-Type": "",
        "Content-Length": responseBody.length
      }
    };

    console.info("Response body:\n", responseBody);

    try {
      await axios.put(event.ResponseURL, responseBody, responseOptions);
      console.info("CloudFormationSendResponse Success");
    } catch (error) {
      console.error("CloudFormationSendResponse Error:");
      if (error.response) {
        console.error(error.response);
      } else if (error.request) {
        console.error(error.request);
      } else {
        console.error("Error", error.message);
      }
      console.error(error.config);
      throw new Error("Could not send CloudFormation response");
    }
  };

  module.exports = {
    seedCountries
  };
