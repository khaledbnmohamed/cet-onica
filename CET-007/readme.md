CET-007

# Task Summary
 # TODO : Make profile and refer to the profile
# TODO:  Pass ami id using varaible
1) Create a new AMI :

   a)  from a base Amazon Linux 2 AMI

   b)  has Apache installed on it

2) use AWS tools for PowerShell to launch an instance with packed AMI


# Steps Needed to launch the instance

## A) Using the `aws-ubuntu.pkr.hcl` included in the repo.
<br>

1) Authenticate to AWS

   Before you can build the AMI, you need to provide your AWS credentials to Packer as environment environments.
   ```bash
   $ export AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY
   $ export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY
   ```

2)  Initialize Packer configuration
      ```bash
      packer init .
      ```
      **Comment: Packer will download the plugin you've defined above. In this case, Packer will download the Packer Amazon plugin that is greater than version**

3) Format and validate your Packer template

   ```bash
   packer fmt .
   packer validate .
   ```

4) Build Packer image
   ```bash
   packer build aws-ubuntu.pkr.hcl
   ```
   **Comment: Packer create instance on the fly, test it with ssh connection and if everything is fine, the image will be created as shown in the images below**

   ![success Image](success.png)
   **If everything went well**

   ![Final Image](final.png)

## B ) Create Instance using AWS tool for PowerShell
<br>

   1) Open powershell using command

      ```bash
      pwsh
      ```
      **Comment: Make sure AWS for PowerShell installed on your CLI**

   2)  The following command creates and launches an Instance with the AMI id we just created and [you added you credentials](https://docs.aws.amazon.com/powershell/latest/userguide/specifying-your-aws-credentials.html)
         ```powershell
         New-EC2Instance -ImageId ami-030bff8ef4f2c456b `
         -MinCount 1 `
         -MaxCount 1 `
         -KeyName khaled_key `
         -SecurityGroups DefaultPublicSGKhaled `
         -InstanceType t2.small
          ```

          Example success response:

          ![Success image](instance.png)


## C ) Try calling the instance ip and hopefully you'll see the apache home page

<br>

# References
* https://learn.hashicorp.com/tutorials/packer/aws-get-started-build-image?in=packer/aws-get-started
* https://www.packer.io/docs/builders/amazon/ebsvolume
* https://www.packer.io/docs/commands/validate
* https://docs.aws.amazon.com/powershell/latest/userguide/pstools-ec2-launch.html
* https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html
